HOST='"host"':'"'"$(hostname)"'"'
DATE='"date"':$(date -u +%s)
CLIENTS='"clients"':$(/usr/local/bin/dhcp-leases.py | grep 'Total Active Leases:'| cut -f 2 -d':'| sed -e 's/^ *//g')
STATE='"state"':'"'"$(/usr/sbin/batctl gw | cut -f 1 -d' ')"'"'
MESSAGE="{$HOST,$DATE,$CLIENTS,$STATE}"
echo $MESSAGE
echo -n $MESSAGE | /usr/sbin/alfred -s 150
