#!/usr/bin/python3

import sys
import json
import socket
import time
import calendar 
import subprocess
import os

status = open('/tmp/joker.state','w')
#is my tunnel up and running?
tunnelState = os.system("ping -c 5 -I tun0 heise.de")
if tunnelState != 0:
	#tunnel is down let us switch gw_mode off 
    subprocess.call(["/usr/sbin/batctl", "gw", "off"])
    status.write('-1')
    print("tunnel is down!!! switching gw_mode off")
else:
    channel= sys.stdin.read()
    gateways=[]
    mySelf = None
    topGw = None
    clientsAvg = 0.0
    gwCount = 0.0
    activGwCount = 0
    for message in channel.split('\n'):
        message = message[24:-4] 
        message = message.replace('\\','')
        if len(message) == 0: continue
        obj = json.loads(message)
        if socket.gethostname() == obj["host"]:
            mySelf = obj
            if int(obj["date"] + 10*60) >= calendar.timegm(time.gmtime()):
                    clientsAvg += obj["clients"]
                    gwCount += 1
            if (topGw == None) or (topGw["clients"] < obj["clients"]):
                topGw = obj
            if mySelf['state'] == 'server':
                activGwCount += 1
        else:
            if int(obj["date"] + 10*60) >= calendar.timegm(time.gmtime()):
                gateways.append(obj)
                gwCount += 1
                clientsAvg += obj["clients"]
            if (topGw == None) or (topGw["clients"] < obj["clients"]):
                topGw = obj
            if obj['state'] == 'server':
                activGwCount += 1
    
    print("All clients:")
    print(clientsAvg)
    if clientsAvg == 0:
        print("no clients")
        exit(-1)
    clientsAvg /= gwCount
    if topGw == mySelf:
        print("i am server more clients than all other gateways")
        print((clientsAvg / mySelf["clients"]) ) 
        if (mySelf["clients"]) * 0.90 > clientsAvg:
            print("i am serving too many clients: ")
            if mySelf['state'] == 'server':
                if activGwCount > 1:
                    print("switching gateway mode off")
                    subprocess.call(["/usr/sbin/batctl", "gw", "off"])
                    status.write('0')
                else:
                    print("i am the only active gateway, staying in gateway mode")
                    status.write('2')
            elif mySelf['state'] == 'off':
                print("waiting for other gateways to take some clients")
                status.write('0')
        else:
            print("but not too much more")
            if mySelf['state'] == 'server':
                print("all ok, i am a gateway")
            elif mySelf['state'] == 'off':
                print("oh I should switch into gateway mode again")
                subprocess.call(["/usr/sbin/batctl", "gw", "server"])
            status.write('1')
    else:
        print("i am just an other gateway")
        if mySelf['state'] == 'server':
            if (mySelf["clients"]) * 0.90 > clientsAvg:
                if activGwCount > 1:
                    print("switching gateway mode off")
                    subprocess.call(["/usr/sbin/batctl", "gw", "off"])
                    status.write('0')
                else:
                    print("i am the only active gateway, staying in gateway mode")
                    status.write('2')
            else:
                print("all ok, i am a gateway")
                status.write('1')
        elif mySelf['state'] == 'off':
            print("oh I should switch into gateway mode again")
            subprocess.call(["/usr/sbin/batctl", "gw", "server"])
            status.write('1')
    print("Clients served by me:")
    print(mySelf['clients'])
    print("Average served clients:") 
    print(clientsAvg)
status.close()
#print("me:")
#print(mySelf)
#print("gateways:")
#print(gateways)
#print("Top gateway:")
#print(topGw)
